
#Part 1 - Building the Conventional Neural Network

from keras.models import Sequential  #initialize the neural network as a sequence of layers
from keras.layers import Convolution2D #first convolution step of CNN 
from keras.layers import MaxPooling2D #pooling step to add pooling layers
from keras.layers import Flatten #flattening step
from keras.layers import Dense #add the fully connected layer in ANN

#Initialize the CNN
classifier = Sequential()

#Step 1 : Convolution
classifier.add(Convolution2D(32,3,3, input_shape = (64,64,3), activation = 'relu'))

#Step 2 : Pooling
classifier.add(MaxPooling2D(pool_size=(2,2)))

#Step 3 : Flattening
classifier.add(Flatten())

#Step 4 : Full Connection
classifier.add(Dense(output_dim = 128, activation = 'relu')) #Hidden Layer
classifier.add(Dense(output_dim = 1, activation = 'sigmoid')) #Output Layer

#Compiling the CNN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

#Part 2 - Fitting CNN to images
from keras.preprocessing.image import ImageDataGenerator
train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

training_set = train_datagen.flow_from_directory(
        'dataset/training_set',
        target_size=(64, 64),
        batch_size=32,
        class_mode='binary')

test_set = test_datagen.flow_from_directory(
        'dataset/test_set',
        target_size=(64, 64),
        batch_size=32,
        class_mode='binary')

classifier.fit_generator(
        training_set,
        steps_per_epoch=8000,  #no of images we have
        epochs=20,
        validation_data=test_set,
        validation_steps=2000)
